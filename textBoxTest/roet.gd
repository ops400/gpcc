extends Node2D

@onready var label = $CanvasLayer/Label
@onready var colorRect = $CanvasLayer/ColorRect
var s: PackedStringArray = [
	"Kill me please.","Daniel is a fofo.","Hello Every Nyan! How Are You?"
]
var canMoveToTheNext: bool = false
var displayString: PackedStringArray
var hasToDisplay: bool = false
var numberOfIterations: int = 0
var iterationNumber:int = 0

func _ready():
	pass

func _process(delta):
	if(hasToDisplay == true):
		label.visible = true
		colorRect.visible = true
		label.text = displayString[iterationNumber]
		if(label.visible_ratio < 1):
			await get_tree().create_timer(0.005).timeout
			label.visible_ratio += 0.01
			canMoveToTheNext = false
		else:
			canMoveToTheNext = true
		if(canMoveToTheNext == true && Input.is_action_just_pressed("next")):
			iterationNumber += 1
			label.visible_ratio = 0
			if(iterationNumber == numberOfIterations):
				hasToDisplay = false
	else:
		iterationNumber = 0
		colorRect.visible = false
		label.visible = false

func _on_button_pressed():
	displayText(s)

func displayText(textPass:PackedStringArray):
#	var numberOfiterations: int = textPass.size() + 1
#	var i: int = 0
#	while(i < numberOfiterations):
#		label.text = textPass[i]
#		while(label.visible_ratio < 1):
#			await get_tree().create_timer(0.005).timeout
#			label.visible_ratio += 0.01
#		if(Input.is_action_just_pressed("next")):
#			print("he was pressed")
#			i += 1
#			label.visible_ratio = 0
#		print("agg")
	label.visible_ratio = 0
	displayString = textPass
	numberOfIterations = textPass.size()
	hasToDisplay = true
	iterationNumber = 0
